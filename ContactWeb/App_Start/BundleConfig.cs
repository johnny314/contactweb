﻿using System.Web;
using System.Web.Optimization;

namespace ContactWeb
{
    public class BundleConfig
    {   
        //package all the stuff in the project and runs it
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            /*
             

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                       // "~/Scripts/jquery.validate*"));




            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                       "~/js/lib/jquery.dataTables.min",
                       "~/js/lib/dataTables.bootstrap.min",
                       "~/js/lib/dataTables.colReorder.min")); 
             
            */

             
           bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
             "~/js/lib/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            "~/js/lib/bootstrap.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                       "~/js/lib/jquery.dataTables.min.js",
                       "~/js/lib/dataTables.bootstrap.min.js",
                       "~/js/lib/dataTables.colReorder.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/js/lib/jquery-ui.min.js"
                ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        //"~/Scripts/modernizr-*"));


            
             bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/css/bootstrap.min.css",
                      //"~/css/bootstrap-theme.min.css",
                      "~/css/jquery.dataTables.min.css",
                      "~/css/jquery.dataTables_themeroller.css",
                      "~/css/dataTables.bootstrap.min.css",
                      "~/css/colReorder.bootstrap.min.css",
                      "~/css/jquery-ui.min.css",
                      "~/css/jquery-ui.structure.min.css",
                      "~/css/jquery-ui.theme.min.css",
                      "~/css/site.css"
                      ));
             
            

        }
    }
}
